package io.songpon.productservice.service;

import io.songpon.productservice.domain.Product;
import io.songpon.productservice.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private InventoryRepository inventoryRepository;

    public Product getProduct(int id) {
        var inventory = inventoryRepository.getInventory(id);
        return new Product(id, "MacbookPro 14", 71000, inventory.getStock());
    }

    public List<Product> listProducts() {
        var products = new ArrayList<Product>();
        products.add(new Product(10001, "Apple Watch Ultra", 31000, inventoryRepository.getInventory(10001).getStock()));
        products.add(new Product(10002, "Sony MH-1005", 11000, inventoryRepository.getInventory(10002).getStock()));
        products.add(new Product(10003, "Dell 4K Monitor", 24000, inventoryRepository.getInventory(10003).getStock()));
        products.add(new Product(10004, "Keycron Keyboard K8", 74000, inventoryRepository.getInventory(10004).getStock()));
        products.add(new Product(10005, "Logitec mouse MX3", 5000, inventoryRepository.getInventory(10005).getStock()));
        products.add(new Product(10006, "Apple iPhone 14 Pro", 41000, inventoryRepository.getInventory(10006).getStock()));
        products.add(new Product(10007, "Google Pixel 6", 24000, inventoryRepository.getInventory(10007).getStock()));
        products.add(new Product(10008, "Google Home mini", 4000, inventoryRepository.getInventory(10008).getStock()));
        products.add(new Product(10009, "Clean Code", 1100, inventoryRepository.getInventory(10009).getStock()));
        products.add(new Product(10010, "Philip Hue", 5000, inventoryRepository.getInventory(10010).getStock()));
        products.add(new Product(10011, "Apple iPad Pro 11", 32000, inventoryRepository.getInventory(10011).getStock()));
        products.add(new Product(10012, "Apple iPad air 5", 26000, inventoryRepository.getInventory(10012).getStock()));
        products.add(new Product(10013, "Netgear Orbi Wifi system", 34000, inventoryRepository.getInventory(10013).getStock()));
        products.add(new Product(10014, "Logitec webcam", 2900, inventoryRepository.getInventory(10014).getStock()));
        products.add(new Product(10015, "Microservice Pattern", 1200, inventoryRepository.getInventory(10015).getStock()));
        products.add(new Product(10016, "The Go Programming language", 1300, inventoryRepository.getInventory(10016).getStock()));
        products.add(new Product(10017, "Spring Microservice in Action", 1200, inventoryRepository.getInventory(10017).getStock()));
        products.add(new Product(10018, "Clean Architecture", 1000, inventoryRepository.getInventory(10018).getStock()));
        products.add(new Product(10019, "JUnit in Action", 1200, inventoryRepository.getInventory(10019).getStock()));
        products.add(new Product(10020, "Sony Play Station 5", 16000, inventoryRepository.getInventory(10020).getStock()));

        return products;
    }
}
