package io.songpon.productservice.repository;

import io.songpon.productservice.domain.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class InventoryRepository {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${inventoryClient.baseUrl}")
    private String inventoryUrl;

    public Inventory getInventory(int id) {
        return restTemplate.getForObject(inventoryUrl + "/v1/inventory/" + String.valueOf(id), Inventory.class);
    }
}
