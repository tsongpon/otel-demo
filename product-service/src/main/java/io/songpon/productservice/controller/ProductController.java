package io.songpon.productservice.controller;

import io.songpon.productservice.domain.Product;
import io.songpon.productservice.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @GetMapping("/v1/products/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable(name = "id")Integer id) {
        logger.info("got product info request id: {}", id);
        var product =  productService.getProduct(id);
        return ResponseEntity.ok(product);
    }

    @GetMapping("/v1/products")
    public ResponseEntity<List<Product>> listProducts() {
        logger.info("listing all product");
        var products = productService.listProducts();
        logger.debug("got {} products", products.size());
        return ResponseEntity.ok(products);
    }
}
