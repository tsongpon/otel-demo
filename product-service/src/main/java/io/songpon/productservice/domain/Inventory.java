package io.songpon.productservice.domain;

public class Inventory {
    private int id;
    private int stock;

    public Inventory(int id, int stock) {
        this.id = id;
        this.stock = stock;
    }

    public Inventory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
