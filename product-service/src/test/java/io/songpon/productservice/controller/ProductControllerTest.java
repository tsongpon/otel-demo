package io.songpon.productservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.util.Random;


class ProductControllerTest {

    @Test
    public void testGetProduct() throws Exception {
        var restTemplate = new RestTemplate();
        while (true) {
            Random random = new Random();

            // Generate a random integer between 1000 and 9000
            int randomNumber = random.nextInt(9001 - 1000) + 1000;

            var sleepTime = random.nextInt(1000 - 10) + 10;
            System.out.println("Getting product info id: "+ randomNumber);
            try {
                var resp = restTemplate.getForObject("http://localhost:8080/v1/products/"+randomNumber, Object.class);
            } catch (Exception e) {
                // intention: ignore
            }
            Thread.sleep(sleepTime);
        }
    }

    @Test
    public void testListProduct() throws Exception{
        var restTemplate = new RestTemplate();
        while (true) {
            Random random = new Random();
            System.out.println("Listing products");
            var resp = restTemplate.getForObject("http://localhost:8080/v1/products", Object.class);
            var sleepTime = random.nextInt(1000 - 10) + 10;
            Thread.sleep(sleepTime);
        }
    }
}