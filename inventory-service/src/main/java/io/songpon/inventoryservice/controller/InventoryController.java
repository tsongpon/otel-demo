package io.songpon.inventoryservice.controller;

import io.songpon.inventoryservice.domain.Inventory;
import io.songpon.inventoryservice.service.InventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryController {
    private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

    @Autowired
    private InventoryService inventoryService;

    @GetMapping("/v1/inventory/{id}")
    public ResponseEntity<Inventory> getInventory(@PathVariable(name = "id") Integer id) {
        logger.info("Got request inventory info for product id {}", id);
        var inventory = inventoryService.getInventory(id);
        if (inventory == null) {
            logger.warn("product id {} is not found", id);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(inventory);
    }

    @GetMapping("/ping")
    public String ping() {
        return "pong";
    }
}
