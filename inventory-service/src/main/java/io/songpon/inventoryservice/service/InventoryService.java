package io.songpon.inventoryservice.service;

import io.songpon.inventoryservice.domain.Inventory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class InventoryService {
    private static final Logger logger = LoggerFactory.getLogger(InventoryService.class);

    public Inventory getInventory(int id) {

        if ((id > 5000 && id < 6000)  || (id > 7000 && id < 8000) ) {
            logger.warn("product id {} is not found", id);
            return null;
        }
        return new Inventory(id, 400);
    }
}
