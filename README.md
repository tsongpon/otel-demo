# Spring Boot OpenTelemetry Demo Applications
Spring Boot applications send application's telemetry data vai OpenTelemetry Java agent

product-service ---get inventory --> inventory-service

## Run application
Build product-service

    mvn clean package

Run product-service (app run at port 8080)
    
    OTEL_LOGS_EXPORTER=otlp OTEL_EXPORTER_OTLP_ENDPOINT="http://localhost:4317" OTEL_RESOURCE_ATTRIBUTES=service.name=product-service java -javaagent:../opentelemetry-javaagent.jar -jar target/*.jar

Build inventory-service

    mvn clean package

Run inventory-service (app run at port 8081)

    OTEL_LOGS_EXPORTER=otlp OTEL_EXPORTER_OTLP_ENDPOINT="http://localhost:4317" OTEL_RESOURCE_ATTRIBUTES=service.name=inventory-service java -javaagent:../opentelemetry-javaagent.jar -jar target/*.jar

## Install and run Signoz for collect and virtualize telemetry data

Following link to install Signoz https://signoz.io/docs/install/docker/
